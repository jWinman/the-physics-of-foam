import numpy as np
import matplotlib.pyplot as plt

U = lambda phi, b: 2 / np.cos(phi) + b - np.tan(phi)
phi = np.linspace(0, np.pi / 4, 1000)
phi0 = 30 * 2 * np.pi / 360

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(phi, U(phi, 1), label=r"$U(\varphi) = a + \frac{2a}{\cos\varphi} - a \tan\varphi$")
ax.plot(phi0, U(phi0, 1),  "ro", markersize=20, label=r"$\varphi_{\mathrm{min}} = 30^{\circ}$")

ax.set_xlabel(r"$\varphi$")
ax.set_ylabel(r"$U / a$")
ax.legend(loc="best")

fig.savefig("minimal_perimeter.pdf")
